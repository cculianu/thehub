Flowee the Hub repository
=========================

![Release pipeline status](https://gitlab.com/FloweeTheHub/thehub/badges/master/pipeline.svg)


# This is theHub repo with Flowee libs and some server apps to get the most out of Bitcoin Cash (BCH)

See more details on https://flowee.org/about/


This repository contains

* The flowee libraries, useful for C++ support for various BCH projects.
* the Hub: the headless (server) Bitcoin Cash node software
* Indexer: Provides various data-stores to speed up lookups on the blockchain.
* hub-cli: the command-line interface to the Hub server.
* bitcore-proxy: a client of hub+indexer to generate the bitcore APIs
* txVulcano: Transaction generator testing application.
* unspentdb: Application to inspect and optimize your UTXO database.


# What is this?

Bitcoin Cash is still a mysterious black box to most people and companies
that would potentially want to use it, or build applications with.

Flowee has been built to help you use Bitcoin Cash. Bring you a platform to
stand on when you simply use or when you build with Bitcoin Cash.

The codebase is derived from the one that originally was created by Satoshi
Nakamoto, the inventor of Bitcoin. This way you know you won't get
compatibility issues. Flowee is adjusted for greatly increased speed and
reliability as well as for scaling up to much larger blocks than the
competition.

# Installation

**To compile and install Flowee libs** on Ubuntu, install the dependencies

`sudo apt install cmake libssl-dev libboost-all-dev qt6-tools-dev-tools qt6-tools-dev`

To compile and install Flowee on MacOS, install the dependencies

`brew install cmake boost qt`

Then clone the repo and use cmake to create the makefile

```
mkdir thehub/build
cd thehub/build
cmake CMakeLists.txt ..
make
make install
```

# Optional Installation of the 'apps'.

As per the introduction, this repo contains more than just the libraries. Applications
like the full node server 'the Hub' are also included.
While they are currently not compiled by default, it is very easy to include them
in your compile.

All you really need to do is replace the one line in the above list of commands:
`cmake -Dbuild_apps=ON CMakeLists.txt ..`

Additionally you will need extra dependencies installed.

For Ubuntu: libevent-dev libminiupnpc-dev pkg-config  
For Brew: libevent miniupnpc pkg-config

# Links

* Upstream: https://codeberg.org/Flowee/thehub
* Website: https://flowee.org
* Twitter: https://twitter.com/floweethehub
* Telegram: https://t.me/Flowee_org
