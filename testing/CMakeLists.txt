# This file is part of the Flowee project
# Copyright (C) 2018 Tom Zander <tom@flowee.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

enable_testing()

get_property(hub_deb GLOBAL PROPERTY BUILDING_HUB)
if (hub_deb)
    add_subdirectory(test)
    set(OTHER_TESTS test_hubOld)
endif()

if (${Qt6Core_FOUND})
    find_package(Qt6Test)
    if (${Qt6Test_FOUND})
        add_subdirectory(common)
        add_subdirectory(apputils)
        add_subdirectory(prevector)
        add_subdirectory(utxo)
        add_subdirectory(utils)
        add_subdirectory(streaming)
        add_subdirectory(networkmanager)
        add_subdirectory(hashstorage)
        add_subdirectory(p2pnet)

        if (hub_deb)
            add_subdirectory(hub)
            add_subdirectory(api)
            add_subdirectory(bitcoin-protocol)
            add_subdirectory(blockvalidation)
            add_subdirectory(doublespend)
            list(APPEND OTHER_TESTS
                test_hub
                test_protocol
                test_blockvalidation
                test_doublespend

                test_api_live
                test_api_blockchain
                test_api_address_monitor
                test_api_double_spend_monitor
                test_api_txid_monitor
            )
        endif()

        find_package(Qt6Network)
        if (${Qt6Network_FOUND})
            add_subdirectory(httpengine)
            list(APPEND OTHER_TESTS test_httpengine)
        endif ()

        option(test_proto_builder "Enable testing of the protocol buffer unit test" OFF)
        if (test-proto_builder)
            find_package(Protobuf REQUIRED)
            if (Protobuf_FOUND)
                add_subdirectory(protobuf)
                LIST (APPEND OTHER_TESTS test_protobuf)
            endif ()
        endif ()

        add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND} DEPENDS
            test_utxo
            test_utils
            test_apputils
            test_prevector
            test_streaming
            test_networkmanager
            test_hashstorage
            test_p2pnet
            ${OTHER_TESTS}
        )
    else ()
        message("Missing qt6 (test) library, not building some tests")
    endif ()
endif()
