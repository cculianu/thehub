/*
 * This file is part of the Flowee project
 * Copyright (C) 2011-2015 The Bitcoin Core developers
 * Copyright (C) 2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASE58_H
#define BASE58_H

#include <common/TestFloweeBase.h>


// Goal: check that parsed keys match test payload
class Base58Tests : public TestFloweeBase
{
    Q_OBJECT
private slots:
    // Goal: check that parsed keys match test payload
    void base58KeysValidParse();
    // Goal: check that generated keys match test vectors
    void base58KeysValidGen();
    // Goal: check that base58 parsing code is robust against a variety of corrupted data
    void base58KeysInvalid();
};

#endif
