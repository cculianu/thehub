/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef P2PNETINTERFACE_H
#define P2PNETINTERFACE_H

#include "PeerAddressDB.h"

/**
 * A listener on actions happening on the DownloadManager / ConnectionManager.
 *
 * \see DownloadManager::addP2PNetListener(P2PNetInterface *listener)
 */
class P2PNetInterface
{
public:
    P2PNetInterface() = default;
    virtual ~P2PNetInterface();

    /// Callback for when the connection manager got a new connection with version message.
    virtual void newPeer(int peerId, const std::string &userAgent, int startHeight, PeerAddress address) {}
    /// Callback for when the connection manger detected a peer no longer connected.
    virtual void lostPeer(int peerId) {}
    /// Callback for when a peers punishment has changed
    virtual void punishmentChanged(int peerId) {}
};

#endif
