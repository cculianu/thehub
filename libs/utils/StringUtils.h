/*
 * This file is part of the Flowee project
 * Copyright (C) 2009-2010 Satoshi Nakamoto
 * Copyright (C) 2009-2016 The Bitcoin Core developers
 * Copyright (C) 2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWEE_STRING_UTILS_H
#define FLOWEE_STRING_UTILS_H

#include <cstdint>
#include <string>
#include <vector>

/*
 * Utilities for converting data from/to strings.
 */
namespace StringUtils
{
    std::vector<char> parseHex(const char *stringData, int size);
    std::vector<char> parseHex(const std::string &source);

    std::string decodeBase64(const std::string &source);
    std::vector<char> decodeBase64(const char *source, bool *ok = nullptr);
    std::string encodeBase64(const char *data, size_t length);
    std::string encodeBase64(const std::string &source);
}

#endif
