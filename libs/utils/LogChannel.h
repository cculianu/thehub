/*
 * This file is part of the Flowee project
 * Copyright (C) 2017-2022 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LOGCHANNEL_H
#define LOGCHANNEL_H

#include <cstdint>
#include <string>

namespace Log {

/**
 * The baseclass for a logging-channel.
 *
 * The logging manager takes logging actions from the application for a full log-line will call
 * the pushLog() method on all registered log-channels.
 *
 */
class Channel {
public:
    enum TimeStampFormat {
        NoTime,
        TimeOnly,
        DateTime
    };
    Channel(TimeStampFormat formatTimestamp);
    virtual ~Channel() {}

    /**
     * A single log-line that is being pushed.
     *
     */
    virtual void pushLog(int64_t timeMillis, std::string *timestamp, const std::string &line, const char *filename,
                         int lineNumber, const char *methodName, short logSection, short logLevel) = 0;

    TimeStampFormat timeStampFormat() const;
    void setTimeStampFormat(const TimeStampFormat &timeStampFormat);

    bool logSubSecondPrecision() const;
    void setlogSubSecondPrecision(bool showSubSecondPrecision);
protected:
    TimeStampFormat m_timeStampFormat;
    bool m_logSubSecondPrecision;
};

}

#endif
