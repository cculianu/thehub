# This file is part of the Flowee project
# Copyright (C) 2018-2021 Tom Zander <tom@flowee.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include_directories(${LIBUTILS_INCLUDES} ${CMAKE_BINARY_DIR}/include)

add_library(flowee_utils STATIC
    amount.cpp
    arith_uint256.cpp
    base58.cpp
    bloom.cpp
    cashaddr.cpp
    chainparamsbase.cpp
    clientversion.cpp
    compat/glibc_sanity.cpp
    compat/glibcxx_sanity.cpp
    compat/strnlen.cpp
    hash.cpp
    HDMasterKey.cpp
    HDMasterPubkey.cpp
    LogChannels.cpp
    Logger.cpp
    Message.cpp
    merkle.cpp
    merkleblock.cpp
    PartialMerkleTree.cpp
    random.cpp
    StringUtils.cpp
    script/interpreter.cpp
    script/script_error.cpp
    streaming/BufferPool.cpp
    streaming/BufferPools.cpp
    streaming/ConstBuffer.cpp
    streaming/MessageBuilder.cpp
    streaming/MessageParser.cpp
    streaming/P2PBuilder.cpp
    streaming/P2PParser.cpp
    streaming/ProtoParser.cpp
    streaming/ProtoBuilder.cpp
    support/cleanse.cpp
    support/lockedpool.cpp
    sync.cpp
    TransactionBuilder.cpp
    primitives/MutableBlock.cpp
    primitives/Block.cpp
    primitives/Tx.cpp
    primitives/key.cpp
    primitives/pubkey.cpp
    primitives/pubkey_utils.cpp
    primitives/script.cpp
    primitives/transaction.cpp
    uint256.cpp
    utilmoneystr.cpp
    utilstrencodings.cpp
    utiltime.cpp
    WorkerThreads.cpp
    WaitUntilFinishedHelper.cpp
)

# allow usage in dlls
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

target_link_libraries(flowee_utils secp256k1 flowee_interfaces flowee_crypto)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libflowee_utils.a DESTINATION lib)

install(FILES
    arith_uint256.h
    base58.h
    bloom.h
    cashaddr.h
    compat.h
    hash.h
    HDMasterKey.h
    HDMasterPubkey.h
    Logger.h
    LogChannel.h
    merkle.h
    Message.h
    PartialMerkleTree.h
    prevector.h
    random.h
    serialize.h
    StringUtils.h
    tinyformat.h
    TransactionBuilder.h
    uint256.h
    WorkerThreads.h
DESTINATION include/flowee/utils/)
install(FILES
    streaming/BufferPool.h
    streaming/BufferPools.h
    streaming/ConstBuffer.h
    streaming/MessageBuilder.h
    streaming/MessageParser.h
    streaming/P2PParser.h
    streaming/P2PBuilder.h
    streaming/ProtoBuilder.h
    streaming/ProtoParser.h
DESTINATION include/flowee/utils/streaming)

install(FILES
    #primitives/MutableBlock.h
    #primitives/Block.h
    primitives/Tx.h
    primitives/key.h
    primitives/pubkey.h
    primitives/pubkey_utils.h
    primitives/script.h
    #primitives/transaction.h
DESTINATION include/flowee/utils/primitives)

install(FILES
    script/interpreter.h
    script/script_error.h
DESTINATION include/flowee/utils/script)

install(FILES support/allocators/secure.h DESTINATION include/flowee/utils/support/allocators)
install(FILES support/lockedpool.h support/cleanse.h DESTINATION include/flowee/utils/support)
